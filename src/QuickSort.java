import java.util.Arrays;

public class QuickSort {
    private static void swap (int[] arr, int i0, int i1) {
        int temp = arr[i0];
        arr[i0] = arr[i1];
        arr[i1] = temp;
    }

    public static int partition (int[] arr, int left, int right) {
        int pivot = arr[right];
        int i = left - 1;

        for (int j = left; j < right; j++) {
            if (arr[j] <= pivot) {
                i++;
                QuickSort.swap(arr, i, j);
            }
        }
        QuickSort.swap(arr, i + 1, right);
        return i + 1;
    }

    public static void quickSort (int[] arr, int left, int right) {
        if (left < right) {
            int pivot = QuickSort.partition(arr, left, right);

            QuickSort.quickSort(arr, left, pivot - 1);
            QuickSort.quickSort(arr, pivot + 1, right);
        }
    }

    public static void main (String[] args) {
        int n = (int) (Math.random() * 10000);
        int[] test = new int[n];

        for (int i = 0; i < test.length; i++) {
            test[i] = (int) (Math.random() * n + 1);
        }

        System.out.println(Arrays.toString(test));
        QuickSort.quickSort(test, 0, test.length - 1);
        System.out.println(Arrays.toString(test));
    }
}
